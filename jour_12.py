
with open('input_12.txt') as f:
    data = list(f.readlines())

def nord_1(valeur, position, orientation):
    position[1] += valeur
    return position, orientation
def sud_1(valeur, position, orientation):
    position[1] -= valeur
    return position, orientation
def est_1(valeur, position, orientation):
    position[0] += valeur
    return position, orientation
def ouest_1(valeur, position, orientation):
    position[0] -= valeur
    return position, orientation
def gauche_1(valeur, position, orientation):
    orientation += valeur
    return position, orientation
def droite_1(valeur, position, orientation):
    orientation -= valeur
    return position, orientation
def devant_1(valeur, position, orientation):
    if orientation % 360 == 0:
        return est_1(valeur, position, orientation)
    elif orientation % 360 == 90:
        return nord_1(valeur, position, orientation)
    elif orientation % 360 == 180:
        return ouest_1(valeur, position, orientation)
    elif orientation % 360 == 270:
        return sud_1(valeur, position, orientation)
    return -1

commandes_1 = {
    'N':nord_1,
    'S':sud_1,
    'E':est_1,
    'W':ouest_1,
    'L':gauche_1,
    'R':droite_1,
    'F':devant_1
}

def nord_2(valeur, balise, position):
    balise[1] += valeur
    return balise, position
def sud_2(valeur, balise, position):
    balise[1] -= valeur
    return balise, position
def est_2(valeur, balise, position):
    balise[0] += valeur
    return balise, position
def ouest_2(valeur, balise, position):
    balise[0] -= valeur
    return balise, position
def gauche_2(valeur, balise, position):
    if valeur % 360 == 180:
        balise[0], balise[1] = -balise[0], -balise[1]
    elif valeur % 360 == 90:
        balise[0], balise[1] = -balise[1], balise[0]
    elif valeur % 360 == 270:
        balise[0], balise[1] = balise[1], -balise[0]
    return balise, position
def droite_2(valeur, balise, position):
    return gauche_2(-valeur, balise, position)
def devant_2(valeur, balise, position):
    position[0] += balise[0] * valeur
    position[1] += balise[1] * valeur
    return balise, position

commandes_2 = {
    'N':nord_2,
    'S':sud_2,
    'E':est_2,
    'W':ouest_2,
    'L':gauche_2,
    'R':droite_2,
    'F':devant_2
}


def challenge_1(data):
    position = [0, 0]
    orientation = 0
    for line in data:
        position, orientation = commandes_1[line[0]](int(line[1:]), position, orientation)
    return sum([abs(x) for x in position])

def challenge_2(data):
    position = [0, 0]
    balise = [10, 1]
    for line in data:
        balise, position = commandes_2[line[0]](int(line[1:]), balise, position)
    return sum([abs(x) for x in position])

print(challenge_1(data))
print(challenge_2(data))
