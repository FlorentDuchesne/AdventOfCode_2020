
with open('input_06.txt', 'r') as f:
    data = f.read().split('\n\n')


def challenge_1(data):
    nb_reponses = 0
    for group in data:
        reponses = set()
        group = group.replace('\n', '')
        for lettre in group:
            reponses.add(lettre)
        nb_reponses += len(reponses)
    return nb_reponses

def challenge_2(data):
    nb_reponses = 0
    for group in data:
        all_reponses = []
        for person in group.split('\n'):
            reponses = set()
            for lettre in person:
                reponses.add(lettre)
            all_reponses.append(reponses)
        nb_reponses += len(set.intersection(*all_reponses))
    return nb_reponses

print(challenge_1(data))
print(challenge_2(data))
