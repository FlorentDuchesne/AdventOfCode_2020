
data = []

with open('input_07.txt') as f:
    for line in f.readlines():
        data.append(line)

def challenge_1(data):
    bag_contains = {}
    for line in data:
        phrase = line.split(' bags contain ')
        contained_bag = phrase[0]
        containers = phrase[1].split(', ')
        for container in containers:
            if len(container.split(' ')) == 4:
                container = container.split(' ')[1] + ' ' + container.split(' ')[2]
                if container not in bag_contains:
                    bag_contains[container] = []
                bag_contains[container].append(contained_bag)
    deja_vu = bag_contains['shiny gold']
    for bag_color in bag_contains['shiny gold']:
        if bag_color in bag_contains:
            liste = bag_contains[bag_color]
            while len(liste):
                for new_color in bag_contains[bag_color]:
                    if new_color not in deja_vu:
                        deja_vu.append(new_color)
                        if new_color in bag_contains:
                            for color in bag_contains[new_color]:
                                liste.append(color)
                    liste.remove(new_color)
    return len(deja_vu)

def get_number_of_bags(color, bag_contains):
    if color not in bag_contains:
        return 0
    total = 0
    for bag in bag_contains[color]:
        total += bag[0] + bag[0] * get_number_of_bags(bag[1], bag_contains)
    return total

def challenge_2(data):
    bag_contains = {}
    for line in data:
        phrase = line.split(' bags contain ')
        container = phrase[0]
        contained_bags = phrase[1].split(', ')
        for bag in contained_bags:
            if len(bag.split(' ')) == 4:
                new_bag = bag.split(' ')[1] + ' ' + bag.split(' ')[2]
                if container not in bag_contains:
                    bag_contains[container] = []
                bag_contains[container].append((int(bag.split(' ')[0]), new_bag))
    return get_number_of_bags('shiny gold', bag_contains)

print(challenge_1(data))
print(challenge_2(data))