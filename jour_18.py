import numpy as np

with open('input_18.txt') as f:
    data = f.readlines()

def parse_line(line, debug = False):
    chiffre = 0
    operation = ''
    prochain_index = 0
    for i, cara in enumerate(line):
        if prochain_index > i:
            continue
        if cara == '(':
            ouvrantes = 0
            fermantes = 0
            for j, c in enumerate(line[i+1:]):
                if c == '(':
                    ouvrantes += 1
                elif c == ')':
                    fermantes += 1
                if fermantes > ouvrantes:
                    if operation == '+':
                        chiffre += parse_line(line[i+1:i+1+j+1])
                    elif operation == '*':
                        chiffre *= parse_line(line[i+1:i+1+j+1])
                    else:
                        chiffre = parse_line(line[i+1:i+1+j+1])
                    prochain_index = i + 2 + j
                    break
        elif cara == ')':
            return chiffre
        elif cara == '+':
            operation = '+'
        elif cara == '*':
            operation = '*'
        else: #chiffre
            if operation == '':
                chiffre = int(cara)
            elif operation == '+':
                chiffre += int(cara)
            elif operation == '*':
                chiffre *= int(cara)
    return chiffre

def index_fin(line):
    fin = len(line)
    ouvrantes = 0
    fermantes = 0
    for i, cara in enumerate(line):
        if cara == '(':
            ouvrantes += 1
        elif cara == ')':
            fermantes += 1
            if fermantes > ouvrantes:
                return i + 1
        elif cara != '+' and cara != '*':
            return i + 1

def erase_index(line, index):
    s = list(line)
    s.pop(index)
    return ''.join(s)

def corresponding_closing(line, i):
    ouvrantes = 0
    fermantes = 0
    for j, c in enumerate(line[i+1:]):
        if c == '(':
            ouvrantes += 1
        elif c == ')':
            fermantes += 1
        if fermantes > ouvrantes:
            return i + j

def challenge_1(data):
    grand_total = 0
    for line in data:
        line = line.replace(' ', '').replace('\n', '')
        total = parse_line(line)
        grand_total += total
    return grand_total

def parse_line_2(line, debug = False):
    chiffre = 0
    operation = ''
    prochain_index = 0
    operations = []
    for i, cara in enumerate(line):
        if prochain_index > i:
            continue
        if cara == '(':
            ouvrantes = 0
            fermantes = 0
            for j, c in enumerate(line[i+1:]):
                if c == '(':
                    ouvrantes += 1
                elif c == ')':
                    fermantes += 1
                if fermantes > ouvrantes:
                    if operation == '+':
                        operations.append((parse_line_2(line[i+1:i+1+j+1]), '+'))
                    elif operation == '*':
                        operations.append(parse_line_2(line[i+1:i+1+j+1]))
                    else:
                        operations += parse_line_2(line[i+1:i+1+j+1])
                    prochain_index = i + 2 + j
                    break
        elif cara == ')':
            return [operations]
        elif cara == '+':
            operation = '+'
        elif cara == '*':
            operation = '*'
        else:
            if operation == '':
                operations += [int(cara)]
            elif operation == '+':
                operations += [(int(cara), '+')]
            elif operation == '*':
                operations += [int(cara)]
    return operations

def apply_operations(operations):
    chiffre = 0
    contains_tuple = True
    while contains_tuple:
        #print(operations)
        contains_tuple = False
        for i, operation in enumerate(operations):
            if type(operation) == list:
                new_operation = apply_operations(operation)
                operations[i] = new_operation
                break
            if type(operation) == int:
                chiffre = operation
            elif type(operation) == tuple:
                if operation[1] == '+':
                    if type(operation[0]) == int:
                        operations[i] = chiffre + operation[0]
                    else:
                        new_operations = apply_operations(operation[0])
                        if type(new_operations) == list:
                            total = 1
                            for i in operations:
                                total *= int
                        elif type(new_operations) == int:
                            total = new_operations
                        operations[i] = chiffre + total
                        operations.pop(i-1)
                        break
                    operations.pop(i-1)
                    break
        for o in operations:
            if type(o) == tuple or type(o) == list:
                contains_tuple = True
                break
    total = 1
    for i in operations:
        total *= i
    #print(total)
    return total

def challenge_2(data):
    grand_total = 0
    for line in data:
        line = line.replace(' ', '').replace('\n', '')
        operations = parse_line_2(line)
        total = apply_operations(operations)
        grand_total += total
    return grand_total


print(challenge_1(data))
print(challenge_2(data))
