import numpy as np

with open('input_16.txt') as f:
    data = [d for d in f.readlines() if d != '\n']

def is_value_valid(value:int, field:list):
    for interval in field:
        if value >= interval[0] and value <= interval[1]:
            return True
    return False

def is_ticket_valid(ticket:str, fields:dict):
    for value in ticket_values(ticket):
        valide = False
        for field_name in fields:
            for interval in fields[field_name]:
                if value >= interval[0] and value <= interval[1]:
                    valide = True
                    break
            if valide:
                break
        if not valide:
            return False, value
    return True, None

def ticket_values(ticket:str):
    return [int(i) for i in ticket.split(',')]

def challenge_1(data):
    i = data.index('your ticket:\n')
    fields = {}
    for line in data[:i]:
        field_name = line.split(':')[0]
        values = line.split(': ')[1].split(' or ')
        fields[field_name] = []
        for val in values:
            v = (int(val.split('-')[0]), int(val.split('-')[1]))
            fields[field_name].append(v)
    invalid_fields = []
    for line in data[i+3:]:
        valid, value = is_ticket_valid(line, fields)
        if not valid:
            invalid_fields.append(value)
    return sum(invalid_fields)

def challenge_2(data):
    i = data.index('your ticket:\n')
    fields = {}
    field_names = []
    for line in data[:i]:
        field_name = line.split(':')[0]
        field_names.append(field_name)
        values = line.split(': ')[1].split(' or ')
        fields[field_name] = []
        for val in values:
            v = (int(val.split('-')[0]), int(val.split('-')[1]))
            fields[field_name].append(v)
    invalid_lines = []
    for j, line in enumerate(data[i+3:]):
        valid, value = is_ticket_valid(line, fields)
        if not valid:
            invalid_lines.append(j)
    tickets = [ticket_values(line) for j, line in enumerate(data[i+3:]) if j not in invalid_lines]
    
    validite = np.zeros((len(fields), len(tickets[0]), len(tickets)), dtype=bool)
    for i, ticket in enumerate(tickets):
        for j, value in enumerate(ticket):
            for k, field_name in enumerate(field_names):
                if is_value_valid(value, fields[field_name]):
                    validite[k, j, i] = True
    fields_valides = {}
    while len(fields_valides) < len(fields):
        for i in range(len(fields)):
            if i in fields_valides:
                continue
            nb_valids = 0
            i_valid = -1
            for j in range(len(tickets[0])):
                if j in fields_valides.values():
                    continue
                if validite[i, j, :].all():
                    nb_valids += 1
                    j_valid = j
                if nb_valids > 1:
                    break
            if nb_valids == 1:
                fields_valides[i] = j_valid
    
    i = data.index('your ticket:\n')
    my_ticket = [int(d) for d in data[i+1].split(',')]
    total = 1
    for i, name in enumerate(field_names):
        if 'departure' in name:
            total *= my_ticket[fields_valides[i]]
    return total


print(challenge_1(data.copy()))
print(challenge_2(data))
