
lines = []

with open('input_02.txt', 'r') as f:
    lines = f.readlines()


def challenge_1(lines):
    nb_valids = 0

    for line in lines:
        min_occurences = int(line.split('-')[0])
        max_occurences = int(line.split('-')[1].split(' ')[0])
        lettre = line.split(' ')[1][0]
        occurences = line.split(':')[1].count(lettre)
        if min_occurences <= occurences and occurences <= max_occurences:
            nb_valids += 1
    return nb_valids

def challenge_2(lines):
    nb_valids = 0

    for line in lines:
        occurence_1 = int(line.split('-')[0])
        occurence_2 = int(line.split('-')[1].split(' ')[0])
        lettre = line.split(' ')[1][0]
        first = line.split(' ')[-1][occurence_1 - 1] == lettre
        second = line.split(' ')[-1][occurence_2 - 1] == lettre
        if first and not second or not first and second:
            nb_valids += 1
    return nb_valids


print(challenge_1(lines))
print(challenge_2(lines))
