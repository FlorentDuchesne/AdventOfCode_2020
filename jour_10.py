import itertools

with open('input_10.txt') as f:
    data = list(f.readlines())
    data = [int(d) for d in data]


def challenge_1(data):
    data.sort()
    data.append(max(data) + 3)
    previous = 0
    ones = 0
    threes = 0
    for line in data:
        if line - previous == 1:
            ones += 1
        elif line - previous == 3:
            threes += 1
        previous = line
    return ones * threes

def get_ancres(data):
    """
    Genere la liste des adaptateurs qui ne peuvent pas etre retires
    (car la difference par rapport a l'adaptateur precedent ou suivant est de 3)
    """
    ancres = [0]
    for i, line in enumerate(data[1:]):
        if line - data[i] == 3 or data[i+2] - line == 3:
            ancres.append(line)
    return ancres

def check_validite(values):
    """
    Verifie que les donnees sont valides (il n'y a pas une difference de plus de 3 entre deux valeurs)
    """
    for i in range(len(values) - 1):
        if values[i+1] - values[i] > 3:
            return False
    return True

def generer_arrangements(ancres, chunks):
    total = 1
    for chunk in chunks:
        nb_valides = 0
        if len(chunk) == 2:
            continue
        for i in range(len(chunk) - 2, -1, -1):
            #genere toutes les combinaisons possibles
            combinaisons = itertools.combinations(chunk[1:-1], i)
            for c in combinaisons:
                values = [chunk[0], *c, chunk[-1]]
                if check_validite(values):
                    nb_valides += 1
        total *= nb_valides
    return total

def challenge_2(data):
    data.sort()
    data.append(max(data) + 3)
    data.insert(0, 0)
    ancres = get_ancres(data)
    chunks = []
    for i, ancre_droite in enumerate(ancres[1:]):
        ancre_gauche = ancres[i]
        chunks.append(data[data.index(ancre_gauche):data.index(ancre_droite) + 1])
    return generer_arrangements(ancres, chunks)


print(challenge_1(data))
print(challenge_2(data))
