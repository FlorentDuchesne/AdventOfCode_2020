from math import ceil

data = []

with open('input_05.txt') as f:
    for line in f.readlines():
        data.append(line)

def get_id(line):
    rangee_min = 0
    rangee_max = 127
    colonne_min = 0
    colonne_max = 7
    for char in line:
        if char == 'B':
            if rangee_max - rangee_min == 1:
                rangee_min = rangee_max
            else:
                rangee_min += ceil((rangee_max - rangee_min) / 2)
        elif char == 'F':
            rangee_max -= ceil((rangee_max - rangee_min) / 2)
        elif char == 'R':
            if colonne_max - colonne_min == 1:
                colonne_min = colonne_max
            else:
                colonne_min += ceil((colonne_max - colonne_min) / 2)
        elif char == 'L':
            colonne_max -= ceil((colonne_max - colonne_min) / 2)
    return rangee_min * 8 + colonne_min

def challenge_1(data):
    id_max = -1
    for line in data:
        current_id = get_id(line)
        if current_id > id_max:
            id_max = current_id
        
    return id_max

def challenge_2(data):
    ids = []
    for line in data:
        ids.append(get_id(line))
    ids.sort()
    for i, id_ in enumerate(ids):
        if ids[i] - ids[i - 1] == 2:
            return ids[i] - 1
    return -1

print(challenge_1(data))
print(challenge_2(data))