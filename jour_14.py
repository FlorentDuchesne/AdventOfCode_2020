
with open('input_14.txt') as f:
    data = list(f.readlines())

def adjust_binary_length(binary_value, mask):
    while len(binary_value) < len(mask):
        binary_value = '0' + binary_value
    return binary_value

def apply_mask_1(decimal_value, mask):
    binary_value = bin(decimal_value).split('b')[1]
    binary_value = adjust_binary_length(binary_value, mask)
    for i, cara in enumerate(mask[-len(binary_value):]):
        if cara != 'X':
            b = list(binary_value)
            b[i] = cara
            binary_value = ''.join(b)
    return int(binary_value, 2)

def challenge_1(data):
    mask = None
    memory = {}
    for line in data:
        if 'mask' in line:
            mask = line.split('=')[-1].strip()
        else:
            index = int(line.split('[')[1].split(']')[0])
            memory[index] = apply_mask_1(int(line.split(' ')[-1]), mask)
    return sum([memory[i] for i in memory])

def generate_addresses(mask):
    if 'X' not in mask:
        return [mask]
    else:
        binary_addresses = []
        index = mask.index('X')
        a = list(mask)
        a[index] = '1'
        a = ''.join(a)
        binary_addresses += generate_addresses(a)
        a = list(mask)
        a[index] = '0'
        a = ''.join(a)
        binary_addresses += generate_addresses(a)
        return binary_addresses

def apply_mask_2(index, mask):
    binary_index = bin(index).split('b')[1]
    binary_index = adjust_binary_length(binary_index, mask)
    for i, cara in enumerate(mask[-len(binary_index):]):
        if cara != '0':
            b = list(binary_index)
            b[i] = cara
            binary_index = ''.join(b)
    addresses = generate_addresses(binary_index)
    addresses = [int(binary_adress, 2) for binary_adress in addresses]
    return addresses

def challenge_2(data):
    mask = None
    memory = {}
    for line in data:
        if 'mask' in line:
            mask = line.split('=')[-1].strip()
        else:
            index = int(line.split('[')[1].split(']')[0])
            addresses = apply_mask_2(index, mask)
            for address in addresses:
                memory[address] = int(line.split(' ')[-1])
    return sum([memory[i] for i in memory])


print(challenge_1(data))
print(challenge_2(data))
