from copy import deepcopy

data = []

with open('input_11.txt') as f:
    for line in f.readlines():
        data.append([])
        for cara in line.strip():
            data[-1].append(cara)

voisins = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]

def print_data(data):
    for row in data:
        print(row)

def should_become_occupied_1(data, i, j):
    for voisin in voisins:
        if i + voisin[0] >= 0 and i + voisin[0] < len(data) and j + voisin[1] >= 0 and j + voisin[1] < len(data[0]):
            if data[i + voisin[0]][j + voisin[1]] == '#':
                return False
    return True

def should_become_empty_1(data, i, j):
    nb_occupied = 0
    for voisin in voisins:
        if i + voisin[0] >= 0 and i + voisin[0] < len(data) and j + voisin[1] >= 0 and j + voisin[1] < len(data[0]):
            if data[i + voisin[0]][j + voisin[1]] == '#':
                nb_occupied += 1
    return nb_occupied >= 4


def should_become_occupied_2(data, i, j):
    for voisin in voisins:
        x = i + voisin[0]
        y = j + voisin[1]
        while x >= 0 and x < len(data) and y >= 0 and y < len(data[0]):
            if data[x][y] == '#':
                return False
            elif data[x][y] == 'L':
                break
            x += voisin[0]
            y += voisin[1]
    return True

def should_become_empty_2(data, i, j):
    nb_occupied = 0
    for voisin in voisins:
        x = i + voisin[0]
        y = j + voisin[1]
        while x >= 0 and x < len(data) and y >= 0 and y < len(data[0]):
            if data[x][y] == '#':
                nb_occupied += 1
                break
            elif data[x][y] == 'L':
                break
            x += voisin[0]
            y += voisin[1]
    return nb_occupied >= 5

def iteration(data, should_become_occupied_function, should_become_empty_function):
    new_data = deepcopy(data)
    changed = False
    for i, row in enumerate(data):
        for j, column in enumerate(row):
            if column == 'L':
                if should_become_occupied_function(data, i, j):
                    changed = True
                    new_data[i][j] = '#'
            elif column == '#':
                if should_become_empty_function(data, i, j):
                    changed = True
                    new_data[i][j] = 'L'
    return new_data, changed

def loop(data, should_become_occupied_function, should_become_empty_function):
    changed = True
    while changed:
        data, changed = iteration(data, should_become_occupied_function, should_become_empty_function)
    return sum([len([d for d in c if d == '#']) for c in data])

def challenge_1(data):
    return loop(data, should_become_occupied_1, should_become_empty_1)
def challenge_2(data):
    return loop(data, should_become_occupied_2, should_become_empty_2)

print(challenge_1(data.copy()))
print(challenge_2(data))
