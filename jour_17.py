import numpy as np

with open('input_17.txt') as f:
    data = f.readlines()

def count_active_neighbours(space, x, y, z):
    return space[x-1:x+2, y-1:y+2, z-1:z+2].sum() - int(space[x, y, z])

def count_active_neighbours_2(space, x, y, z, w):
    return space[x-1:x+2, y-1:y+2, z-1:z+2, w-1:w+2].sum() - int(space[x, y, z, w])

def challenge_1(data):
    space = np.zeros((len(data[0]) + 8 * 2, len(data[0]) + 8 * 2, len(data[0]) + 8 * 2), dtype=bool)
    zero = int(space.shape[0] / 2)
    input_center = int(len(space) / 2 - len(data) / 2) + 1
    for i, line in enumerate(data):
        for j, char in enumerate(line):
            if char == '#':
                space[i + input_center, j + input_center, input_center + 1] = True
    new_space = space.copy()
    for _ in range(6):
        for x in range(1, space.shape[0] - 1):
            for y in range(1, space.shape[1] - 1):
                for z in range(1, space.shape[2] - 1):
                    if space[x, y, z]:
                        count = count_active_neighbours(space, x, y, z)
                        if count != 2 and count != 3:
                            new_space[x, y, z] = False
                    else:
                        if count_active_neighbours(space, x, y, z) == 3:
                            new_space[x, y, z] = True
        space = new_space.copy()
    return space.sum()

def challenge_2(data):
    space = np.zeros((len(data[0]) + 8 * 2, len(data[0]) + 8 * 2, len(data[0]) + 8 * 2, len(data[0]) + 8 * 2), dtype=bool)
    zero = int(space.shape[0] / 2)
    input_center = int(len(space) / 2 - len(data) / 2) + 1
    for i, line in enumerate(data):
        for j, char in enumerate(line):
            if char == '#':
                space[i + input_center, j + input_center, input_center + 1, input_center + 1] = True
    new_space = space.copy()
    for _ in range(6):
        for x in range(1, space.shape[0] - 1):
            for y in range(1, space.shape[1] - 1):
                for z in range(1, space.shape[2] - 1):
                    for w in range(1, space.shape[3] - 1):
                        if space[x, y, z, w]:
                            count = count_active_neighbours_2(space, x, y, z, w)
                            if count != 2 and count != 3:
                                new_space[x, y, z, w] = False
                        else:
                            if count_active_neighbours_2(space, x, y, z, w) == 3:
                                new_space[x, y, z, w] = True
        space = new_space.copy()
    return space.sum()


print(challenge_1(data))
print(challenge_2(data))
