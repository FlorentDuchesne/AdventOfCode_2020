data = []

with open('input_08.txt') as f:
    for line in f.readlines():
        data.append(line)

def challenge_1(data):
    accumulator = 0
    i = 0
    past_lines = []
    while i < len(data):
        if i in past_lines:
            break
        past_lines.append(i)
        line = data[i]
        instruction, value = line.split(' ')
        value = int(value)
        if instruction == 'acc':
            accumulator += value
        if instruction == 'jmp':
            i += value
        else:
            i += 1
    return accumulator

def challenge_2(data):
    valide = False
    modification_index = 0
    while modification_index < len(data):
        past_lines = []
        accumulator = 0
        i = 0
        d = data.copy()
        while 'acc' in d[modification_index]:
            modification_index += 1
        if 'jmp' in d[modification_index]:
            d[modification_index] = d[modification_index].replace('jmp', 'nop')
        elif 'nop' in d[modification_index]:
            d[modification_index] = d[modification_index].replace('nop', 'jmp')
        modification_index += 1
        while i not in past_lines:
            if i >= len(d):
                return accumulator
            past_lines.append(i)
            line = d[i]
            instruction, value = line.split(' ')
            value = int(value)
            if instruction == 'acc':
                accumulator += value
            if instruction == 'jmp':
                i += value
            else:
                i += 1
    return None

print(challenge_1(data))
print(challenge_2(data))
