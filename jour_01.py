import csv


data = []

with open('input_01.csv', 'r') as f:
    reader = csv.reader(f)
    for line in reader:
        data.append(int(line[0]))

def challenge_1(data):
    for i, n in enumerate(data):
        for m in data[i:]:
            if n + m == 2020:
                return n * m
    return -1

def challenge_2(data):
    for i, n in enumerate(data):
        for j, m in enumerate(data[i:]):
            for o in data[j:]:
                if n + m + o == 2020:
                    return n * m * o
    return -1

print(challenge_1(data))
print(challenge_2(data))

