
data = []

with open('input_03.txt', 'r') as f:
    for line in f.readlines():
        data.append(line.strip())

def calculate_hits(pente, data):
    position = [0, 0]
    arbres = 0
    while position[1] < len(data):
        if position[0] >= len(data[0]):
            position[0] -= len(data[0])
        if data[position[1]][position[0]] == '#':
            arbres += 1
        position[0] += pente[0]
        position[1] += pente[1]
    return arbres

def challenge_1(data):
    pente = [3, 1]
    return calculate_hits(pente, data)

def challenge_2(data):
    pentes = [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]
    total = 1
    for pente in pentes:
        total *= calculate_hits(pente, data)
    return total

print(challenge_1(data))
print(challenge_2(data))