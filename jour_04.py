
with open('input_04.txt', 'r') as f:
    data = f.read().split('\n\n')

def challenge_1(data):
    champs = ["byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"]
    nb_valid = 0
    for passeport in data:
        valid = True
        for champ in champs:
            if champ not in passeport:
                valid = False
                break
        if valid:
            nb_valid += 1
    return nb_valid

def check_byr(passeport):
    s = passeport[passeport.index("byr:") + 4:].split(' ')[0].split('\n')[0]
    return len(s) == 4 and int(s) >= 1920 and int(s) <= 2002
def check_iyr(passeport):
    s = passeport[passeport.index("iyr:") + 4:].split(' ')[0].split('\n')[0]
    return len(s) == 4 and int(s) >= 2010 and int(s) <= 2020
def check_eyr(passeport):
    s = passeport[passeport.index("eyr:") + 4:].split(' ')[0].split('\n')[0]
    return len(s) == 4 and int(s) >= 2020 and int(s) <= 2030
def check_hgt(passeport):
    s = passeport[passeport.index("hgt:") + 4:].split(' ')[0].split('\n')[0]
    if len(s) == 4:
        return s[-2:] == 'in' and int(s[:2]) >= 59 and int(s[:2]) <= 76
    elif len(s) == 5:
        return s[-2:] == 'cm' and int(s[:3]) >= 150 and int(s[:2]) <= 193
    return False
def check_hcl(passeport):
    s = passeport[passeport.index("hcl:") + 4:].split(' ')[0].split('\n')[0]
    if len(s) != 7 or s[0] != '#':
        return False
    for i in range(1, 7):
        if s[i] > 'f' or s[i] < '0':
            return False
    return True
def check_ecl(passeport):
    s = passeport[passeport.index("ecl:") + 4:].split(' ')[0].split('\n')[0]
    valides = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
    for v in valides:
        if s == v:
            return True
    return False
def check_pid(passeport):
    s = passeport[passeport.index("pid:") + 4:].split(' ')[0].split('\n')[0]
    return len(s) == 9 and int(s) >= 0
def challenge_2(data):
    nb_valid = 0
    for passeport in data:
        try:
            if not check_byr(passeport):
                continue
            if not check_iyr(passeport):
                continue
            if not check_eyr(passeport):
                continue
            if not check_hgt(passeport):
                continue
            if not check_hcl(passeport):
                continue
            if not check_ecl(passeport):
                continue
            if not check_pid(passeport):
                continue
            nb_valid += 1
        except:
            pass
    return nb_valid

print(challenge_1(data))
print(challenge_2(data))