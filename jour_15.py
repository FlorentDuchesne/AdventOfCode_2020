
with open('input_15.txt') as f:
    data = f.readline().split(',')
    data = [int(d) for d in data]

def challenge_1(data):
    spoken_numbers = data
    for i in range(len(data), 2020):
        last_number = spoken_numbers[-1]
        if spoken_numbers.count(last_number) == 1:
            spoken_numbers.append(0)
        else:
            last_time_spoken = len(spoken_numbers) - spoken_numbers[::-1].index(last_number)
            previous_time_spoken = len(spoken_numbers[:last_time_spoken-1]) - spoken_numbers[:last_time_spoken-1][::-1].index(last_number)
            spoken_numbers.append(last_time_spoken - previous_time_spoken)
    return spoken_numbers[-1]

def challenge_2(data):
    previous = {}
    for i, d in enumerate(data):
        previous[d] = [i+1]
    last = data[-1]
    for i in range(len(previous), 30000000):
        turn = i + 1
        if len(previous[last]) == 1:
            previous[0].append(turn)
            last = 0
        else:
            n = previous[last][-1] - previous[last][-2]
            if n not in previous:
                previous[n] = [turn]
            else:
                previous[n].append(turn)
            if len(previous[n]) > 2:
                previous[n] = previous[n][-2:]
            last = n
    return last


print(challenge_1(data.copy()))
print(challenge_2(data))
