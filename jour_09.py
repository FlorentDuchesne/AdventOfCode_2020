data = []

with open('input_09.txt') as f:
    for line in f.readlines():
        data.append(line)

def check_valid(x:list, y:int):
    for i in range(len(x)):
        for j in range(i, len(x)):
            if x[i] + x[j] == y:
                return True
    return False

def challenge_1(data, n=25):
    preamble = [int(x) for x in data[:n]]
    for line in data[n:]:
        if not check_valid(preamble, int(line)):
            return int(line)
        del preamble[0]
        preamble.append(int(line))
    return -1


def challenge_2(data, n=14360655):
    for i, line in enumerate(data):
        liste = [int(line)]
        j = i
        while sum(liste) < n:
            j += 1
            liste.append(int(data[j]))
        if sum(liste) == n:
            return min(liste) + max(liste)
    return 0

print(challenge_1(data))
print(challenge_2(data, challenge_1(data)))
