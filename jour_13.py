
with open('input_13.txt') as f:
    data = list(f.readlines())

def challenge_1(data):
    timestamp = int(data[0])
    autobus = data[1].strip().split(',')
    autobus = [int(a) for a in autobus if a != 'x']
    temps_min = timestamp
    bus_min = timestamp
    for bus in autobus:
        temps = bus - timestamp % bus
        if temps < temps_min:
            temps_min = temps
            bus_min = bus
    return temps_min * bus_min

def trouver_ppcm(autobus:list, differences:list):
    """
    Trouve les plus petits communs multiples pour chaque sous-ensemble d'autobus de gauche a droite.
    Trouve, dans l'ordre, le PPCM du bus a l'index 0, puis des bus [0:2], [0:3], etc.
    """
    old_ppcm = autobus[0]
    ppcm = []
    for i in range(1, len(autobus) + 1):
        valide = False
        timestamp = 0
        while not valide:
            timestamp += old_ppcm
            valide = True
            for bus, diff in zip(autobus[0:i], differences[0:i]):
                if timestamp % bus != 0:
                    valide = False
                    break
            if valide:
                ppcm.append(timestamp)
                old_ppcm = timestamp
    return ppcm

def trouver_bons_departs(autobus:list, differences:list, ppcm:list):
    """
    Trouve, pour chaque sous-ensemble des bus, l'intervalle depuis le dernier PPCM qui est valide.
    Commence au PPCM de l'index 0 pour trouver le timestamp valide pour les bus [0:2]
    Part de ce timestamp et fait des bonds de PPCM[0] pour trouver le timestamps valide des bus [0:3]
    Fait ensuite des bonds de PPCM[1] pour trouver le timestamp valide des bus [0:4], etc.
    """
    timestamp = 0
    for i in range(1, len(autobus) + 1):
        valide = False
        while not valide:
            valide = True
            for bus, diff in zip(autobus[0:i], differences[0:i]):
                if (timestamp + diff) % bus != 0:
                    valide = False
                    timestamp += ppcm[i - 2]
                    break
    return timestamp

def challenge_2(data):
    autobus = data[1].strip().split(',')
    differences = [] #differences de temps entre deux autobus (sert a compter les 'x')
    for i, bus in enumerate(autobus):
        if bus != 'x':
            differences.append(i)
    autobus = [int(a) for a in autobus if a != 'x']
    ppcm = trouver_ppcm(autobus, differences)
    timestamp = trouver_bons_departs(autobus, differences, ppcm)
    return timestamp


print(challenge_1(data))
print(challenge_2(data))
